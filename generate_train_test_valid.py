# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 18:34:09 2015

@author: rAyyy

This code will create 3 files:
Training, validation and testing set
We pick randomly
The size of the validation and testing are quite smaller to be able to load them completely in the ram
"""

import pandas as pd
import zipfile
import json,csv
import numpy as np
import time
import random
import cPickle

import os
script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in

#create validation_test_and training set
chunk_size=1000


training_set = "data/new_set.csv"
abs_file_path = os.path.join(script_dir, training_set)     
iter_csv = pd.read_csv(abs_file_path,
                     iterator=True, chunksize=chunk_size)
                     

first_test=True
first_valid=True
first_train=True

p_test=0.05
p_validate=0.05
for chunk in iter_csv:
    
    #change number of picked trips depending size chunk
    row=random.sample(chunk.index,int(chunk_size*(p_test+p_validate  )))
    for_test = row[:len(row)/2]
    for_valid = row[len(row)/2:]
    
    
    chunk_test=chunk.ix[for_test]
    chunk_valid=chunk.ix[for_valid]
    chunk_train=chunk.drop(row)
    
    training_set = "data/test_rand.csv"
    abs_file_path = os.path.join(script_dir, training_set) 
    with open(abs_file_path, 'a') as f:
            chunk_test.to_csv(f, header=first_train)
    
    training_set = "data/validate_rand.csv"
    abs_file_path = os.path.join(script_dir, training_set) 
    with open(abs_file_path, 'a') as f:
            chunk_valid.to_csv(f, header=first_train)
            
    training_set = "data/train_rand.csv"
    abs_file_path = os.path.join(script_dir, training_set) 
    with open(abs_file_path, 'a') as f:
            chunk_train.to_csv(f, header=first_valid)
            first_valid=False

    first_train=False
    
