# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 11:39:12 2015

@author: rAyyy

Model used by the winning team of the challenge


This code is inspired from the tutorial of deep learning of theano
"""

import theano
import theano.tensor as T
from theano import pp
from theano.compile.nanguardmode import NanGuardMode
import theano.sandbox.cuda as cuda

import copy

import matplotlib.pyplot as plt
from mlp import HiddenLayer

import sys,os
import cPickle
import pickle
import pandas as pd
import zipfile
import json
import numpy as np
import numpy as np
import timeit

import os
script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in

from collections import OrderedDict

def detect_nan(i, node, fn):
    #print "######### DEBUG"
    for output in fn.outputs:
        if (not isinstance(output[0], np.random.RandomState) and
                np.isnan(output[0]).any()):
            print('*** NaN detected ***')
            theano.printing.debugprint(node)
            print('Inputs : %s' % [np.asarray(input[0]) for input in fn.inputs])
            print('Outputs: %s' % [np.asarray(output[0]) for output in fn.outputs])
            break
        
def Predict():
    """
    Load the last saved MCT  (Model car team) then predict the destination for the test file
    """
    MCT = cPickle.load(open('MCT_model.pkl'))
        
    print "...load dico"
    training_set = "data/dictionaries.pk"
    abs_file_path = os.path.join(script_dir, training_set) 
    with open(abs_file_path, 'r') as f:
        dico=cPickle.load( f)
        
    print "...load test data"
    test_set="new_converted_test.csv"
    df_test = pd.read_csv(test_set,converters={'POLYLINE': lambda x: scaler.fit_transform( np.array(json.loads(x))   ),
                 })
    
    #Meta data input
    X_prediction_meta=df_test[dico.keys()+['WEEK_DAY','QUARTER','WEEK']]
    
    #GPS data inputs
    prediction_set_x=np.asarray([ np.concatenate((p[:k],p[-k:])) for p in df_test['POLYLINE'].values ])
    
    #if we dont have enough inputs we create them
    new=[]
    for elmt in prediction_set_x.T:
        while len(elmt)<k*2:
            elmt=np.concatenate((elmt,[2*elmt[-1]-elmt[-2]]))
        new.append(elmt)
    new=np.asarray(new)
    
    #create the flat array input
    prediction_set_x=np.asarray([ np.ravel( p) for p in new ])
     
    ne=[['ORIGIN_CALL',57106],['TAXI_ID',448],['ORIGIN_STAND',64],['QUARTER',96],
    ['WEEK_DAY',7],['WEEK',52],['CALL_TYPE',3]]
    
    #create the input in the right order for the meta data
    z=[]
    for elmt,size in ne:
        z.append(X_prediction_meta[elmt].values)
    
    z=np.asarray(z,'int64')
    
    
    #create the dictionary for the evaluation of the theano function
    trial_prediction={}
    for i,elmt in enumerate(ne):
        trial_prediction[MCT.input[1][i]]= z[i]
    p=dict(trial_prediction)
    
    trial_prediction[MCT.input[0]]=prediction_set_x
    
    #do the prediction
    end = MCT.result.eval(trial_prediction)
    end=np.array([end[1],end[0]])
    end=end.T#final prediction array
    
    #save it
    samp_sub='data/sampleSubmission.csv.zip'
    abs_file_path = os.path.join(script_dir, samp_sub)    
    zf = zipfile.ZipFile(abs_file_path)
    df_sub = pd.read_csv(zf.open('sampleSubmission.csv'))
    
    df_sub[['LATITUDE','LONGITUDE']]=end
    
    samp_sub='result/submition_neural_net.csv'
    with open(samp_sub, 'w') as f:
                df_sub.to_csv(f, header=True,index=False)
                
                
                
class model_car_team():
    """
    This class provide all the elements for the model
    It contain the parameters, clusters and function
    """
    def __init__(self, rng, input,n_hidden, n_out,clusters,ne,de,k):
        
        self.input = input
        self.ne=ne
        self.de=de
        self.k=k
        self.clusters=clusters
        
        
        embeding_parameters=[]
        var_list=[]
        for i,elmt in enumerate(ne):
            self.emb = theano.shared(name='embeddings_param_'+elmt[0],
                                             value=np.asarray(0.2 * np.random.uniform(-1.0, 1.0,
                                             (elmt[1]+1, de)) ,dtype=theano.config.floatX))
                                             
                                             
            embeding_parameters.append(self.emb)
            p = self.emb[input[1][i]]
            var_list.append(p)    
        
        #regroup all inputs
        self.var_list=var_list+[input[0]]
        self.concatenated_emb_var=T.concatenate(self.var_list,axis=1)
        
        self.embeding_parameters=embeding_parameters
        
        
        def ReLu(x):
            return T.switch(x<0,0,x)
        
        #inputs to hidden layer
        self.hlayer1 = HiddenLayer(
                rng,
                input=self.concatenated_emb_var ,
                n_in= k*4+de *7,
                n_out=n_hidden,        
                activation=ReLu#T.tanh
            )
        
        #hidden layer to softmax layer
        self.hlayer2 = HiddenLayer(
                rng,
                input= self.hlayer1.output,
                n_in=n_hidden,#batch_size*2,
                n_out=n_out,           #temporary
                activation=T.nnet.softmax#T.tanh
            )
            
        #self.softmax=T.nnet.softmax(self.hlayer2.output)
        #T.nnet.softmax(np.array([[4.,2.],[5.,6.]],dtype='float32')
        #self.result= T.dot(self.softmax,clusters).T
        
        #temp=T.switch(self.hlayer2.output<1e-25,0.0,self.hlayer2.output)
        #calculate the prediction based on the calculated clusters
        
        self.result= T.dot(self.hlayer2.output,clusters).T
        #self.result= T.dot(temp,clusters).T
        
        self.parameters=self.embeding_parameters+self.hlayer1.params+self.hlayer2.params

def load_file_X(path):
    """
    load file with defined converters
    those file have already been converted with the dictionary
    """
    df_test = pd.read_csv(path,
                 converters={'POLYLINE': lambda x: scaler.fit_transform (json.loads(x)[0]  ),
                 'Y':lambda x: json.loads(x)
                 })
    return df_test
    
def share_data(path):
    """
    Load the file and share the memory for theano function
    Here only used for the validation set and test set
    """
    df_test1 = load_file_X(path)
    chunk=df_test1
    
    #meta data
    X_training_meta=chunk[dico.keys()+['WEEK_DAY','QUARTER','WEEK']]
    
    #create an array for mlp inputs
    train_set_x     = np.asarray([ np.ravel(p) for p in chunk['POLYLINE'].values])
    
    #for the cost function
    train_set_y=np.asarray([ p for p in chunk['Y'].values])
    
    #share those data
    train_set_x1=theano.shared(np.asarray(train_set_x,dtype=theano.config.floatX),
                                     borrow=borrow)
    train_set_y1=theano.shared(np.asarray(train_set_y,dtype=theano.config.floatX),
                                     borrow=borrow)
    
    return X_training_meta , train_set_x1 , train_set_y1

######################################################################################
"""
Define variables
"""
borrow=True #theano shared variables

k=5 #number of prefix selected (warning already choosed in create_file_linebyline)
R=6371.0 #km

learning_rate=0.01
n_epochs=50
batch_size=500

chunk_size=1500000


rng = np.random.RandomState(23455)

#####################################################################################
"""
Load data
"""

print "... load data:"

if 'scaler' not in locals():
    print "...load scaler"
    training_set = "data/scaler.pkl"
    abs_file_path = os.path.join(script_dir, training_set)    
    with open(abs_file_path, 'rb') as f:
         scaler,=cPickle.load(f)

if 'dico' not in locals():
    print "...load dico"
    training_set = "data/dictionaries.pkl"
    abs_file_path = os.path.join(script_dir, training_set) 
    with open(abs_file_path, 'r') as f:
        dico=cPickle.load( f)

if 'cluster_set' not in locals():
    print "... load clusters"
    #data about clusters
    cluster_set='data/cluster_mean_with_kmean.pickle'
    abs_file_path = os.path.join(script_dir, cluster_set) 
    pkl_file = open(cluster_set, 'r')
    clusters,=pickle.load(pkl_file)
    '''
    lat_low, lat_hgh = np.percentile(clusters[:,0], [10, 90])
    lon_low, lon_hgh = np.percentile(clusters[:,1], [10, 90])
    clusters = np.array([elmt for elmt in clusters if ( elmt[0] >= lat_low and elmt[0] <= lat_hgh and elmt[1] >= lon_low and elmt[0] <= lon_hgh)])
    '''
    clusters=theano.shared(np.asarray(clusters,dtype=theano.config.floatX),
                                     borrow=borrow)


if 'X_testing_meta' not in locals():
    print "... load testing set"
    #data for testing
    test_set="data/test_rand.csv"
    abs_file_path = os.path.join(script_dir, test_set) 
    X_testing_meta , X_testing , Y_testing=share_data(abs_file_path)
    
if 'X_validation_meta' not in locals():
    print "... load validation set"
    #data for validation
    valid_set="data/validate_rand.csv"
    abs_file_path = os.path.join(script_dir, valid_set) 
    X_validation_meta,  X_validation ,Y_validation =share_data(valid_set)   
    
#####################################################################################      
print "... building the model"

#number of clusters for the model output
n_out=clusters.get_value(borrow=True).shape[0]

# compute number of minibatches for validation and testing
n_valid_batches = X_validation.get_value(borrow=True).shape[0]
n_test_batches = X_testing.get_value(borrow=True).shape[0]
##
n_valid_batches /= batch_size
n_test_batches /= batch_size


#embeding parameters
ne=[['ORIGIN_CALL',57106],['TAXI_ID',448],['ORIGIN_STAND',64],['QUARTER',96],
    ['WEEK_DAY',7],['WEEK',52],['CALL_TYPE',3]]
    
#size of the embeding
de=10


#list of variables for the meta data inputs
var_index=[]
for elmt in ne:
    idxs = T.lvector(name='embeddings_indx_var_'+elmt[0])
    var_index.append(idxs)
    
    
#batch inputs
index = T.lscalar()


#GPS Inputs
x = T.matrix('x') 
y = T.matrix('y')#for comparaison (cost)


#share embedings  data for validation and test
var_list=[]
trial={}
trial_train={}
trial_test={}
trial_valid={}
for i,elmt in enumerate(ne):
    
    
    temp_X_testing_meta=T.cast(theano.shared(np.asarray(X_testing_meta[elmt[0]],dtype=theano.config.floatX),
                                     borrow=borrow), 'int64')
    temp_X_validation_meta=T.cast(theano.shared(np.asarray(X_validation_meta[elmt[0]],dtype=theano.config.floatX),
                                     borrow=borrow), 'int64')
    
    
    trial_test[var_index[i]]=temp_X_testing_meta[index * batch_size: (index + 1) * batch_size]
    trial_valid[var_index[i]]=temp_X_validation_meta[index * batch_size: (index + 1) * batch_size]


trial_test[x]=X_testing[index * batch_size: (index + 1) * batch_size]
trial_test[y]=Y_testing[index * batch_size: (index + 1) * batch_size]

trial_valid[x]=X_validation[index * batch_size: (index + 1) * batch_size]
trial_valid[y]=Y_validation[index * batch_size: (index + 1) * batch_size]


MCT=model_car_team(rng,[x,var_index],500,n_out,clusters,ne,de,k )

cost=T.mean( R*T.sqrt(  T.sqr( (y.T[0]-MCT.result[0])* T.cos( (y.T[1]-MCT.result[1])/2  ) ) +  T.sqr( y.T[1]-MCT.result[1] ) )    )

test_model = theano.function(
    [index],
    cost,
    givens=trial_test
)

validate_model = theano.function(
    [index],
    cost,
    givens=trial_valid
)

# create a list of all model parameters to be fit by gradient descent
params=MCT.parameters
# create a list of gradients for all model parameters
grads=T.grad(cost, params)


#create SGD with momemtum updates
momentum=0.8
updates = OrderedDict()
for param,grad in zip(params,grads):
        mparam = theano.shared(param.get_value()*0.)
        updates[mparam] = mparam*momentum +  learning_rate*grad
        updates[param] = param -  mparam # param -  mparam#T.switch(mparam*momentum +  learning_rate*grad == 'nan',mparam,mparam*momentum +  learning_rate*grad)
        


###############
# TRAIN MODEL #
###############
print '... training'

# early-stopping parameters
patience = 20000  # look as this many examples regardless
patience_increase = 2  # wait this much longer when a new best is
                       # found
improvement_threshold = 0.995  # a relative improvement of this much is
                               # considered significant
validation_frequency = min(7000, patience)
                              # go through this many
                              # minibatche before checking the network
                              # on the validation set; in this case we
                              # check every epoch

compteur=0
minibatch_avg_cost=0

old_val1=0
old_val2=0
old_val3=0
old_val4=0
old_val5=0
 
best_validation_loss = np.inf
best_iter = 0
test_score = 0.
start_time = timeit.default_timer()

epoch = 0
done_looping = False

while (epoch < n_epochs) and (not done_looping):
    epoch = epoch + 1
    
    """
    To manage the memory the training file is cut in chunk file
    We will iterate on them during the all training
    """
    training_set = "data/train_rand.csv"
    abs_file_path = os.path.join(script_dir, training_set)    
    iter_training = pd.read_csv(abs_file_path,converters={'POLYLINE': lambda x: scaler.fit_transform (json.loads(x)[0]  ),#scaler.fit_transform(json.loads(x)[:k]+json.loads(x)[k:]),
                     'Y': lambda x: json.loads(x)  },
                         iterator=True, chunksize=chunk_size)
    
    print '.. load partial training'  
    for mini_chunk_index,chunk in enumerate(iter_training):     
        
        print iter,mini_chunk_index    
        print "compteur ", compteur
        
        #number of batches of this data load
        n_train_batches = chunk.shape[0]/batch_size
        
        #meta data and gps data from training file
        X_training_meta=chunk[dico.keys()+['WEEK_DAY','QUARTER','WEEK']]
        train_set_x=np.asarray([ np.ravel(p) for p in chunk['POLYLINE'].values])
        
        #real destination for cost computation
        train_set_y=np.asarray([ p for p in chunk['Y'].values])
        
        #share
        train_set_x1=theano.shared(np.asarray(train_set_x,dtype=theano.config.floatX),
                                         borrow=borrow)
        train_set_y1=theano.shared(np.asarray(train_set_y,dtype=theano.config.floatX),
                                         borrow=borrow)
        
        
        #for theano function evaluation                 
        for i,elmt in enumerate(ne):
            trial[var_index[i]]=np.asarray(X_training_meta[elmt[0]],dtype='int64')[0:100]
            temp_X_training_meta=T.cast(theano.shared(np.asarray(X_training_meta[elmt[0]],dtype=theano.config.floatX),
                                             borrow=borrow), 'int64')
        
            trial_train[var_index[i]]=temp_X_training_meta[index * batch_size: (index + 1) * batch_size]
        
        trial[x]=train_set_x1.get_value()[0:100]
        #trial[y]=train_set_y1.get_value()[0:100]
        trial_train[x]=train_set_x1[index * batch_size: (index + 1) * batch_size]
        trial_train[y]=train_set_y1[index * batch_size: (index + 1) * batch_size]
        
    
        
        
        #cost evaluation fuction for this chunk file
        train_model = theano.function(
                    [index],
                    cost,
                    name='training_function',
                    updates=updates,
                    givens=trial_train,#NanGuardMode(nan_is_error=True, inf_is_error=True, big_is_error=True)
                    mode=theano.compile.MonitorMode(post_func=detect_nan)#NanGuardMode(nan_is_error=True, inf_is_error=True, big_is_error=True)#theano.compile.MonitorMode(post_func=detect_nan)
                )

        #we now iterate on all batchs for SGD
        for minibatch_index in xrange(n_train_batches):
            
            '''
            if minibatch_index<=2 or minibatch_index >=n_train_batches-3:
                print "##### BEFORE batch " , minibatch_index, ' / ', n_train_batches, ' chunk ',mini_chunk_index
                print 'epoch ', epoch
                print minibatch_avg_cost
                print MCT.embeding_parameters[0].get_value()[0]
                print MCT.hlayer2.output.eval(trial)
                
            if compteur%300==0:
                print MCT.hlayer2.output.eval(trial)
                B=(MCT.result.T[0:7]).eval(trial)
                A=(y[0:7]).eval({y:train_set_y1.get_value()[0:100]})
                plt.figure()
                for elmta,elmtb in zip(A,B):
                    vect=np.concatenate(([elmta],[elmtb]),axis=0)
                    plt.plot(vect.T[0],vect.T[1])
                #plt.scatter(clusters.get_value().T[0],clusters.get_value().T[1])
                image_name='graphs/prediction_development_chunk='+str(mini_chunk_index)+'_epoch='+ str(epoch)+'_batch='+str(minibatch_index)+ ' on '+ str(n_train_batches)+'.jpg'
                abs_file_path = os.path.join(script_dir, image_name) 
                plt.savefig(abs_file_path)
                plt.close()
            
                      
            oldold_val1=copy.deepcopy(old_val1)
            oldold_val2=copy.deepcopy(old_val2)
            oldold_val3=copy.deepcopy(old_val3)
            oldold_val4=copy.deepcopy(old_val4)
            oldold_val5=copy.deepcopy(old_val5)
            
            #old_val=MCT.embeding_parameters[0].get_value()[0]
            old_val1=copy.deepcopy(MCT.embeding_parameters[0].get_value())
            old_val2=copy.deepcopy(MCT.hlayer2.output.eval(trial))
            old_val3=copy.deepcopy(MCT.hlayer2.b.get_value())
            old_val4=copy.deepcopy(minibatch_avg_cost)
            old_val5=copy.deepcopy(MCT.hlayer1.b.get_value())
            '''
            
            
            #calculate cost and do updates of parameters
            minibatch_avg_cost = train_model(minibatch_index)
            
            
            '''
            if minibatch_index<=2 or minibatch_index >=n_train_batches-3:
                print "#####AFTER batch " , minibatch_index, ' / ', n_train_batches, ' chunk ',mini_chunk_index
                print 'epoch ', epoch
                print minibatch_avg_cost
                print MCT.embeding_parameters[0].get_value()[0]
                
            if np.isnan(minibatch_avg_cost).any():
                print "BREAKKK"
                print "break ##### batch " , minibatch_index, ' / ', n_train_batches, ' chunk ',mini_chunk_index
                print 'epoch ', epoch
                print minibatch_avg_cost
                print MCT.embeding_parameters[0].get_value()[0]
                print MCT.hlayer2.b.get_value()
                break
            '''            
            
            compteur+=1
            iter = (mini_chunk_index)* n_train_batches+ minibatch_index #dont take in account chunk size

            
            
            #save the model regulary
            if (compteur+1)%4000==0:
                with open('MCT_model.pkl', 'w') as f:
                     cPickle.dump(MCT, f)
            
            
            if (compteur + 1) % validation_frequency == 0:
                    # compute loss on validation set
                    validation_losses = [validate_model(i) for i
                                         in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)
    
                    print(
                        'epoch %i, chunk%i, minibatch %i/%i, validation error %f ' %
                        (
                            epoch,
                            mini_chunk_index,
                            minibatch_index + 1,
                            n_train_batches,
                            this_validation_loss
                        )
                    )
    
                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:
                        #improve patience if loss improvement is good enough
                        if (
                            this_validation_loss < best_validation_loss *
                            improvement_threshold
                        ):
                            patience = max(patience, iter * patience_increase)
    
                        best_validation_loss = this_validation_loss
                        best_iter = iter
    
                        # test it on the test set
                        test_losses = [test_model(i) for i
                                       in xrange(n_test_batches)]
                        test_score = np.mean(test_losses)
    
                        print(('     epoch %i, minichunk %i, minibatch %i/%i, test error of '
                               'best model %f ') %
                              (epoch, mini_chunk_index,minibatch_index + 1, n_train_batches,
                               test_score ))
    
            if patience <= iter:
                done_looping = True
                break
        '''
        if np.isnan(minibatch_avg_cost).any():
                print "chunk BREAKKK"
                break
        print '.. load partial training'
    if np.isnan(minibatch_avg_cost).any():
        print "epoch BREAKKK"
        break
        '''
    

end_time = timeit.default_timer()
print(('Optimization complete. Best validation score of %f  '
       'obtained at iteration %i, with test performance %f ') %
      (best_validation_loss , best_iter + 1, test_score ))
print >> sys.stderr, ('The code for file ' +
                      os.path.split(__file__)[1] +
                      ' ran for %.2fm' % ((end_time - start_time) / 60.))
                      

        
        
'''if __name__ == '__main__':
    sgd_optimization_mnist()'''
    
    