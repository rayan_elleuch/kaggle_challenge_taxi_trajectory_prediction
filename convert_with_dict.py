# -*- coding: utf-8 -*-
"""
Created on Sat Sep 19 05:19:46 2015

@author: rAyyy

This code will create a new training set with all string data converted in index
We also remove unused data
"""

import pandas as pd
import zipfile
import json,csv
import numpy as np
import time
import cPickle

import os
script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in

#create validation_test_and training set
chunk_size=10000

if 'dico' not in locals():
    print "...load dico"
    dico = 'data/dictionaries.pkl'
    abs_file_path = os.path.join(script_dir, dico)
    with open(abs_file_path, 'r') as f:
        dico=cPickle.load( f)
        
train_set = 'data/train.csv.zip'  #or test
abs_file_path = os.path.join(script_dir, train_set)
zf = zipfile.ZipFile(abs_file_path)
iter_csv = pd.read_csv(zf.open('train.csv'),converters={'POLYLINE': lambda x: json.loads(x)
                    ,'TIMESTAMP': lambda x: time.gmtime(float(x)),
           'ORIGIN_CALL':  lambda x: str(10) + x,
                'ORIGIN_STAND':  lambda x: str(10) + x,
                    'CALL_TYPE':lambda x : ord(x)
                 },
                     iterator=True, chunksize=chunk_size)

first_train=True

for chunk in iter_csv:
    
    chunk['WEEK_DAY'] = chunk['TIMESTAMP'].apply (lambda x: x.tm_wday)
    chunk['QUARTER'] = chunk['TIMESTAMP'].apply (lambda x: (x.tm_hour*60+x.tm_min)/15)
    chunk['WEEK'] = chunk['TIMESTAMP'].apply (lambda x: x.tm_yday/7)
    del chunk['TIMESTAMP']
    del chunk['DAY_TYPE']
    del chunk['MISSING_DATA']
    del chunk['TRIP_ID']
    chunk=chunk.fillna('NaN')
    
    #convert with the already created dictionary
    for elmt in dico.keys():
        chunk=chunk.replace({elmt:dico[elmt]})
        
    #if the value is not in the dictionary
    #we set default value
    a=chunk['ORIGIN_CALL']>len(dico['ORIGIN_CALL'])
    a=a==True
    chunk.ORIGIN_CALL[a]=0
    
    chunk.ORIGIN_STAND[a]=0


    train_set = 'data/new_converted_train.csv'  #or test
    abs_file_path = os.path.join(script_dir, train_set)
    with open(abs_file_path, 'a') as f:
            chunk.to_csv(f, header=first_train)
    

    
    
    first_train=False
    