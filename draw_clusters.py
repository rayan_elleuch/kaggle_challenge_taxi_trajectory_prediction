# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 11:05:44 2015

@author: rAyyy
"""
                                     
import matplotlib.pyplot as plt
import itertools
import colorsys
import time
import pickle
import pandas as pd
import zipfile
import json,csv
import numpy as np
from sklearn import preprocessing
import cPickle

from sklearn import cluster
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler
import matplotlib.cm as cm

import os
script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in

colors=iter(cm.rainbow(np.linspace(0,1,350)))

colors=iter(itertools.cycle(["b","k","c"])) 


if 'cluster_set' not in locals():
    print "... load clusters"
    #data about clusters
    train_set = 'data/cluster_mean_with_kmean.pickle'
    cluster_set = os.path.join(script_dir, train_set)
    pkl_file = open(cluster_set, 'r')
    clusters,=pickle.load(pkl_file)
                                     
train_set = 'data/train.csv.zip'
abs_file_path = os.path.join(script_dir, train_set)
zf = zipfile.ZipFile(abs_file_path)
df_test = pd.read_csv(zf.open('train.csv'),converters={'POLYLINE': lambda x: json.loads(x)},
                     iterator=True, chunksize=10000) 

for chunk in df_test:
    Y=[p[-1:] for p in chunk['POLYLINE'].values if len(p)>0]
    for elmt in Y:
        elmt=elmt[0]
        color = next(colors)
        plt.scatter(elmt[0],elmt[1],color='k',s=0.1)

for elmt in clusters:
    plt.scatter(elmt[0],elmt[1],color='r',s=5)

    



