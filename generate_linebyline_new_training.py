# -*- coding: utf-8 -*-
"""
Created on Sat Sep 19 05:10:41 2015

@author: rAyyy

create from the converted training all the possible trajetories
(here limited to 25% 50% 75% 100% of each trips)
"""
import csv
import pandas as pd
import zipfile
import json,csv
import numpy as np
import time
import random
import cPickle

import os
script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in

chunksize=10000

first_train=True

#25, 50, 75, 100   %
number_of_part=4
    

test_set = "data/new_converted_train.csv"
abs_file_path = os.path.join(script_dir, test_set)
with open(abs_file_path,'r') as f:
        iter_df = pd.read_csv(f,converters={'POLYLINE': lambda x: json.loads(x)},
                                           iterator=True, chunksize=chunksize)
        for chunk in iter_df:
            all_trj=chunk['POLYLINE'].values
            
            for i in range(0,number_of_part):
                all_n=[]
                for traj in all_trj:
                    lengh=len(traj)
                    size=int(lengh*1/number_of_part)
                    
                    new_traj=traj[0:(i+1)*size]
                    
                        
                    #print new_traj
                    k=5
                    
                    train_set_x=np.concatenate((new_traj[:k],new_traj[-k:])) 
                    n=[]
                    vect=train_set_x
                    while vect.shape[0]!=k*2:#if we dont have 10 values we create the missing one with a very simple interpolation
                        vect=np.concatenate((vect,[2*vect[-1]-vect[-2]]))
                    n.append(vect.tolist())
                    all_n.append(n)
                    
                #we still need the value to create the others possible trips
                temp_chunk=chunk.copy()
                temp_chunk['POLYLINE']=all_n
                
                test_set = "data/new_set.csv"
                abs_file_path = os.path.join(script_dir, test_set)
                with open(abs_file_path, 'a') as f:
                    temp_chunk.to_csv(f, header=first_train)
            
                first_train=False
                
                
                