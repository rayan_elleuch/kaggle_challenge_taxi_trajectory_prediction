# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 23:14:46 2015

@author: rAyyy

this code will create scaler.pkl and 
"""

#from sklearn import cluster
import matplotlib.pyplot as plt

import colorsys
import time
import pickle
import pandas as pd
import zipfile
import json,csv
import numpy as np
from sklearn import preprocessing
import cPickle

from sklearn import cluster
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler

import os
script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in

if 'df' not in locals():
    train_set = 'data/train.csv.zip'  #or test
    abs_file_path = os.path.join(script_dir, train_set)
    zf = zipfile.ZipFile(abs_file_path)
    df = pd.read_csv(zf.open('train.csv'),
                 converters={'POLYLINE': lambda x: json.loads(x)[-1:],
                 'TIMESTAMP': lambda x: time.gmtime(float(x))})
    X=np.asarray([x[0] for x in df['POLYLINE'].values if len(x)>0])

    #delete very far value
    lat_low, lat_hgh = np.percentile(X[:,0], [5, 95])
    lon_low, lon_hgh = np.percentile(X[:,1], [5, 95])
    X = np.array([elmt for elmt in X if ( elmt[0] >= lat_low and elmt[0] <= lat_hgh and elmt[1] >= lon_low and elmt[0] <= lon_hgh)])



scaler = preprocessing.StandardScaler().fit(X)

final_path='data/scaler.pkl'
abs_file_path = os.path.join(script_dir, final_path)
with open(abs_file_path, 'wb') as f:
     cPickle.dump([scaler], f)


#if 'km' not in locals():
km=cluster.KMeans(n_clusters=3000)

# estimate bandwidth for mean shift
#bandwidth = cluster.estimate_bandwidth(X, quantile=0.3,n_samples=50000)

# create clustering estimators
#ms = cluster.MeanShift(bandwidth=bandwidth, bin_seeding=True)
    


clustering_algorithms = [km]
clustering_names=['kmean']

#if you want to try different clusters algorithm (here just Kmean)
for name,algorithm in zip(clustering_names,clustering_algorithms):
    
    algorithm.fit(X)
    
    centers = algorithm.cluster_centers_
    
    final_path='data/cluster_mean_with_'+name +'.pickle'
    abs_file_path = os.path.join(script_dir, final_path)
    with open(final_path, 'w') as f:
        pickle.dump([centers], f)

'''
for name,algorithm in zip(clustering_names,clustering_algorithms):
    #algorithm.fit(X)
    
    y_pred = algorithm.labels_
    centers = algorithm.cluster_centers_
    
    final_path='cluster_mean_with_'+name +'.pickle'
    with open(final_path, 'w') as f:
            pickle.dump([centers], f)
    
    lat_low, lat_hgh = min(X[:,0])-1,max(X[:,0])+1
    lon_low, lon_hgh = min(X[:,1])-1,max(X[:,1])+1
    lat_low1, lat_hgh1 = np.percentile(X[:,0], [2, 98])
    lon_low1, lon_hgh1 = np.percentile(X[:,1], [2, 98])
    for i,elmt in enumerate([[lat_low, lat_hgh,lon_low, lon_hgh,10,100], [lat_low1, lat_hgh1,lon_low1, lon_hgh1,0.1,100]]):
        lat_low, lat_hgh,lon_low, lon_hgh,s1,s2=elmt
        plt.figure()
        
        h= (lat_hgh-lat_low)/1000
        xx, yy = np.meshgrid(np.arange(lat_low, lat_hgh, h), np.arange(lon_low, lon_hgh, h))
        Z = algorithm.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)
        
        plt.imshow(Z, interpolation='nearest',
           extent=(xx.min(), xx.max(), yy.min(), yy.max()),
           cmap=plt.cm.Paired,
           aspect='auto', origin='lower')
        
   
        maxo=max(y_pred)
        colors=[]
        for i in y_pred:
            a=float( y_pred[i]  )/float(maxo) 
            color_temp=colorsys.hls_to_rgb((a),0.5,1.0)
            colors.append(np.asarray(color_temp))
        colors=np.asarray(colors)
        
        #plt.scatter(X[:, 0], X[:, 1], color=colors[y_pred], s=10)
        x=np.array([elmt for elmt in X if elmt[0] >lat_low and elmt[0] <lat_hgh and elmt[1] >lon_low and elmt[1] <lon_hgh])
        plt.scatter(x[:, 0], x[:, 1],c='k',s=s1)
        
        
        
        maxo=len(centers)
        colors=[]
        for i,elmt in enumerate(centers):
            a=float( i  )/float(maxo) 
            color_temp=colorsys.hls_to_rgb((a),0.5,1.0)
            colors=np.append(np.asarray(color_temp),[0.5])
        plt.scatter(centers[:, 0], centers[:, 1], s=100, c=colors)
        
        clu=np.array([elmt for elmt in centers if elmt[0] >lat_low and elmt[0] <lat_hgh and elmt[1] >lon_low and elmt[1] <lon_hgh])
        plt.scatter(clu[:, 0], clu[:, 1],
                    marker='x', s=s2, linewidths=2,
                    color='w', zorder=10)
        
        
        
        plt.savefig('cluster_mean_with_'+name+'_res='+ str(i)+'_' +'.jpg')  
'''