There is 2 main model:

analyse.py

and

train_MCT.py

########################################################

For analyse.py
to run it please download the training set  train.csv.zip on the website and place it in the directory ../data

the script will create plot in the graph subdirectory and change the submission file in the result subdirectory


For train_MCT.py

I compressed the created trajectories. Please uncompress those ending with .rar and let them directly in the directory ../data

#####
If you want to create those trajectories

you will need to create the data that will be used for the training
we need to generate partial trajectory

For this you will need the file train.csv.zip  from the website

If dictionaries.pkl, cluster_mean_with_kmean.pickle and scaler.pickle
are not present in the data subdirectory build them by running:
create_dictionaries.py
create_cluster_file.py

To convert the training data with the dictionary run convert_with_dict.py  
it will create the file new_converted_train.csv

Then run generate_linebyline_new_training.py to create a file with the new trajectories

Finally run  generate_train_valid_test.py to have the file for the training algorithm in MCT_training.py

If you want to predict the test file with this model use the Predict function

#######################"
The code was used on windows 8 using winpython 2.7 64bit


