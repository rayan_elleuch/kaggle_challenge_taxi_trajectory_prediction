# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 18:30:26 2015

@author: rAyyy

Create file dictionaries.pkl
"""
import cPickle
import pickle
import pandas as pd
import zipfile
import json,csv
import numpy as np
import time
import numpy as np
import math

import os
script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in

if 'df_train' not in locals():
    #here we can load the all file because we dont take the GPS informations
    train_set = 'data/train.csv.zip'  #or test
    abs_file_path = os.path.join(script_dir, train_set)
    zf = zipfile.ZipFile(abs_file_path)
    df_train = pd.read_csv(zf.open('train.csv'),
                     converters={'TIMESTAMP': lambda x: time.gmtime(float(x)),
                                 'CALL_TYPE': lambda x : ord(x),
                        'ORIGIN_CALL':  lambda x: str(10) + x,
                        'ORIGIN_STAND':  lambda x: str(10) + x,
                     'POLYLINE': lambda x: np.array(json.loads(x)[:1])}
                     )
    
    df_train=df_train.fillna('NaN')

#create the dictionary
voca_dico={}
for elmt in ['CALL_TYPE','ORIGIN_CALL','ORIGIN_STAND','TAXI_ID']:
    a=df_train[elmt].value_counts()#count all possible value
    a=a.sort_index()
    if elmt =='ORIGIN_CALL':
        b=a[a>15].reset_index()   #limit to person calling more than x times
    else:
        b=a.reset_index()
    b[0]=b.index
    b.set_index('index').T
    voca_dico[elmt]=b.set_index('index').to_dict()[0]

#save
dico = 'data/dictionaries.pkl'  #or test
abs_file_path = os.path.join(script_dir, train_set)
with open(abs_file_path, 'wb') as f:
                 cPickle.dump(voca_dico, f)
                 
