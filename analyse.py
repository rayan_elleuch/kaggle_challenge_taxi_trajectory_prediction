# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 06:44:13 2015

@author: rAyyy


This file will compute a prediction for each line in the test file.
To do so it will read for each trajectory the training set to look for related trajectory.
"""

import matplotlib.pyplot as plt

from dtw import dtw


from sklearn.neighbors import KernelDensity

import pandas as pd
import zipfile
import json
import numpy as np

import os
script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in

import bisect

import scipy.interpolate as interp

from math import sqrt
from rdp import rdp
import itertools
import matplotlib.cm as cm



from math import radians, cos, sin, asin, sqrt,atan

def haver(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * atan(sqrt(a)) 
    r = 6371000 # Radius of earth in kilometers. Use 3956 for miles
    return c * r


chunk_size=100000 #We load this many row at each iteration from the training set

colors_train = iter(itertools.cycle(["b","k","c"])) 
colors=iter(cm.rainbow(np.linspace(0,1,350)))

#We load the test file
test_set = "data/test.csv.zip"
abs_file_path = os.path.join(script_dir, test_set)
zf = zipfile.ZipFile(abs_file_path)
df_test = pd.read_csv(zf.open('test.csv'),
                 converters={'POLYLINE': lambda x: np.array(json.loads(x))})
                 
                 
#for each trajectory in the test file we will do a prediction
for i,test_trip in enumerate(df_test['POLYLINE'].values):
    if i<=60:      #If you wish to start from a specific line in the test file
        continue
    print i
    plt.figure(i)
    
    color=next(colors)
    
    row=test_trip.T
    
    #Plot the partial trajectory
    plt.scatter(row[0][0],row[1][0],color='g',s=200) #start
    plt.scatter(row[0][-1],row[1][-1],color='r',s=200) #end
    plt.scatter(row[0],row[1],color=color,s=3)
    
    test_trip_lenght=len(test_trip) # test_trip lenght
    if test_trip_lenght>6: #If we have information about the trajectory
        a=rdp(test_trip,0.000009) #Reduce the number of point to keep the main information

        if len(a)>3:  #We need at leat 4 points to compute an interpolation
            test_trip=a
        else:
            #Remove all duplicated points
            polyline=test_trip
            duplicates = []
            for i in range(1, len(polyline)):
                print np.allclose(polyline[i], polyline[i-1])
                if np.allclose(polyline[i], polyline[i-1]):
                    duplicates.append(i)
            if duplicates:
                polyline = np.delete(polyline, duplicates, axis=0)
            test_trip=polyline
            
        #Do an interpolation
        tck, u = interp.splprep(test_trip.T,s=0)
        u = np.linspace(0.0, 1.0, 1000)
        B=interp.splev(u, tck)
        
        #plot it
        plt.plot(B[0],B[1],color=color)
        
        #load partial part of the training set  at each iteration
        #We can't load the all data set in the RAM
        train_set = 'data/train.csv.zip'
        abs_file_path = os.path.join(script_dir, train_set)
        zf = zipfile.ZipFile(abs_file_path)
        iter_csv = pd.read_csv(zf.open('train.csv'),converters={'POLYLINE': lambda x: json.loads(x)
                         },
                             iterator=True, chunksize=chunk_size)
        
        #All collected trajectory
        sorted_list_of_trips=[]
        for p,chunk in enumerate(iter_csv):
            
            '''
            #If we have collected enough data we stop
            if len(list_of_selected_trajectories)>500:
                #This is actually wrong we should read the all file and collecte the nearest one
                #I did it to speed up the computation time
                print "stoped earlier"
                break
            else:
            '''
            
            #for each trip present in the partial load of training trip
            for train_trip in chunk['POLYLINE'].values:
                #train trip lenght
                train_trip_lenght=len(train_trip)
                
                #We first look for trajectory longer than the testing one
                if train_trip_lenght!=0 and train_trip_lenght>=test_trip_lenght*1.1 :
                    #We then compute the haversine distance from the start of the test trip
                    #to the start of the train trip
                    #print train_trip_lenght,len(train_trip)
                    lon,lat =train_trip[0] #longitude and latitude of start of train trip
                    lon_test,lat_test=test_trip[0][0], test_trip[0][1]#longitude and latitude of start of test trip
                    dist=haver(lon, lat,lon_test,lat_test)
                    
                    #algorithm  for 2000 nearest start point
                    #we sort from the right because most points are far from our starting point
                    if len(sorted_list_of_trips)==0:
                        sorted_list_of_trips=[[train_trip,dist]]
                    elif len(sorted_list_of_trips)<=2000:
                        indx=bisect.bisect_right( np.array(sorted_list_of_trips)[:,1], dist)
                        sorted_list_of_trips.insert(indx,[train_trip,dist])
                        #print len(sorted_list_of_trips)
                    elif dist< sorted_list_of_trips[-1:][0][1]:
                        indx=bisect.bisect_right( np.array(sorted_list_of_trips)[:,1], dist)
                        sorted_list_of_trips.insert(indx,[train_trip,dist])
                        del sorted_list_of_trips[-1]
                            
                        """
                        #if it is close enough
                        if dist<25:
                            #We now compute the distance from the last known position of the test trip
                            #to all position of the training trip
                            train_trip=rdp(train_trip,0.00011)#create an approximation of the train trip
                            min_dist_end=5000
                            lon_test,lat_test=test_trip[-1][0], test_trip[-1][1]
                            for lon,lat in train_trip:
                                dist=haver(lon, lat,lon_test,lat_test)
                                #print dist
                                min_dist_end= min(dist,min_dist_end)
                            #if it close enough
                            if min_dist_end<25:
                                #We now check if the final point of the training trip is near the test trajectory
                                #If we passed in front of it it is not a good trajectory
                                lon_train,lat_train=train_trip[-1][0], train_trip[-1][1]
                                dist_end_to_end=haver(lon_test,lat_test,lon_train,lat_train)
                                min_on_all_test_to_train=dist_end_to_end
                                for lon,lat in test_trip[:-1]:
                                    dist=haver(lon, lat,lon_train,lat_train)
                                    min_on_all_test_to_train= min(dist,min_on_all_test_to_train)
                                #If the closest point to the end of the train trip is the last point of the test trip it is good
                                if min_on_all_test_to_train>dist_end_to_end*0.8:
                                    #add the trajectory to the collection
                                    this_trajectory=np.array(train_trip)
                                    list_of_selected_trajectories.append(this_trajectory)
                                """
                                
        #we now filter the selected trips
                                
        list_of_selected_trajectories=[] 
        list_of_selected_trajectories=np.array(sorted_list_of_trips)[:,0]
        
        #If the closest point to the end of the train trip is the last point of the test trip we select it
        #the taxi drive shoud go in the good direction
        new=[]
        for train_trip in list_of_selected_trajectories:
            lon_test,lat_test=test_trip[-1][0], test_trip[-1][1]
            lon_train,lat_train=train_trip[-1][0], train_trip[-1][1]
            dist_end_to_end=haver(lon_test,lat_test,lon_train,lat_train)
            min_on_all_test_to_train=dist_end_to_end
            for lon,lat in test_trip[:-1]:
                dist=haver(lon, lat,lon_train,lat_train)
                min_on_all_test_to_train= min(dist,min_on_all_test_to_train)
            
            if min_on_all_test_to_train>dist_end_to_end*0.8:
                #add the trajectory to the collection
                new.append(train_trip)
        
        #Compute a score for each trajectory
        # if 2 trips are close to each other the score will be low
        #(this is not a  very good solution) note: shoud look for trip that pass by the end of the test trip (heavy computation)
        # to scale limit to selected trip base on start
        cost=[]
        for elmt in list_of_selected_trajectories:
            cost.append(dtw(test_trip,elmt)[0] / train_trip_lenght)
        mean=[]
        
        #we plot the best trips
        infx,maxx=lon_test,lon_test
        infy,maxy=lat_test,lat_test
        for indx in np.array(cost).argsort()[:50]:
            row=np.array(list_of_selected_trajectories[indx])
            mean.append(row[-1])
            row=row.T
            infx,maxx= min(infx,min(row[0])),max(maxx,max(row[0]))
            infy,maxy= min(infy,min(row[1])),max(maxy,max(row[1]))
            plt.scatter(row[0][0],row[1][0],color='g',s=20)
            plt.scatter(row[0][-1],row[1][-1],color='r',s=20)
            plt.scatter(row[0],row[1],color=next(colors_train),s=0.05)
        
        
        
        #all selected trajectories to compute the prediction with a mean
        mean=np.asarray(mean)
        
        #if nothing take the last known position of the test trip
        if len(mean)==0:
            mean=np.array([test_trip[-1]])
            
        #This part was to study the density distribution of the best trajectories
        '''
        else:
            #kde = KernelDensity(bandwidth=0.001, metric='haversine',kernel='gaussian', algorithm='ball_tree')
            kde.fit(mean)
            xgrid = np.linspace(infx, maxx, 2000)
            ygrid= np.linspace(infy, maxy, 2000)
            
            X, Y = np.meshgrid(xgrid, ygrid)
            xy = np.vstack([X.ravel(), Y.ravel()]).T
            Z=kde.score_samples(xy)        
            Z = Z.reshape(X.shape)
            
            
            
            levels=np.linspace(-150,max(kde.score_samples(mean)),20)
            #plt.contourf(X, Y, Z,levels=levels , cmap=plt.cm.Blues)
            plt.contour(X, Y, Z, levels=levels)
            
            best=np.argsort(kde.score_samples(mean))[-1]
        #result=np.array(mean)[best]'''
            
        #prediction
        result= np.array(mean).mean(axis=0)
        
        #plot the result and trajectories
        try:
            plt.scatter(result[0],result[1],color='b',s=300)
            df_sub = pd.read_csv("submition_analysis_way.csv")
            df_sub.loc[i:i,'LATITUDE':'LONGITUDE']=[result[1],result[0]]
            
            
            result = 'data/submition_analysis_way.csv'
            abs_file_path = os.path.join(script_dir, result)
            with open(abs_file_path, 'w') as f:
                df_sub.to_csv(f, header=True,index=False)
            print "succeed"
        except:
            print 'error while saving'
        print 'done' 
        
        #save the plot
        img = 'graphs/partial_trajectory_'+str(i)+'_+possibilities'+'.jpg'
        abs_file_path = os.path.join(script_dir, img)
        plt.savefig(abs_file_path)
        plt.close()